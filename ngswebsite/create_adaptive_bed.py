"""Annotate with UCSC Genome Browser."""

__author__ = "Martin Haagmans (https://gitlab.com/MartinHaagmans)"
__license__ = "MIT"

import logging

import pymysql
import pybedtools

logging.basicConfig(format='%(levelname)s: %(message)s')

class GetGeneRegion:
    """Annotate gene or region with RefGene.

    Method to query gene names returns list of genomic intervals.
    """

    def __init__(self, genes, flank=15000, strandspecific=False, host=None, user=None, passwd=None, db=None):
        """Establish database connection."""
        if host is None and user is None and passwd is None:
            host = 'genome-mysql.cse.ucsc.edu'
            user = 'genome'
        if passwd is None:
            passwd = ''
        if db is None:
            db = 'hg38'
        elif db == 'hg38':
            db = 'hg38'            
        elif db == 'hg19':
            logging.ERROR("Computer says no")
        elif db == 'T2T':
            # Table name different for T2T (not refgene)
            db = 'hs1'
            logging.ERROR("Computer says sorry")            
        self.host = host
        self.user = user
        self.db = db
        self.passwd = passwd
        self.conn = pymysql.connect(
            host=self.host, user=self.user,
            passwd=self.passwd, db=self.db
            )
        self.c = self.conn.cursor()
        self.flank = flank
        self.strandspecific = strandspecific
        self.genes = genes
        
    def get_region(self, gene, tx=True, cds=False):
        """Read gene name2 and return list of tuples.

        Parse gene name2 and return list of tuples with coding (cds) or
        transcription (tx) region for all NM-numbers of that gene.
        """
        if cds:
            tx = False

        txsql = """SELECT DISTINCT chrom, txStart, txEnd, strand
        FROM refGene
        WHERE name2='{g}'
        """.format(g=gene)

        cdssql = """SELECT DISTINCT chrom, cdsStart, cdsEnd, strand
        FROM refGene
        WHERE name2='{g}'
        """.format(g=gene)

        if tx and not cds:
            self.c.execute(txsql)
        elif cds and not tx:
            self.c.execute(cdssql)
        generegions = list()
        for i in self.c.fetchall():
            region = i[0], i[1], i[2], i[3]
            generegions.append(region)
        return generegions

    def parse_genelist_file(self):
        genes = list()
        with open(self.fn_genes) as f:
            for line in f:
                gene = line.rsplit()[0]
                genes.append(gene)
        return genes

    def get_max_gene_region(self, gene):
        regions = self.get_region(gene)
        
        start = None
        end = None

        for region in regions:
            chrom, _start, _end, strand = region

            if start is None:
                start = _start
            elif _start < start:
                start = _start
            
            if end is None:
                end = _end
            elif _end > end:
                end = _end

        return (chrom, start, end, strand)

    def flank_region(self, region, strand):
        flanked = list()
        start, end = region

        if not self.strandspecific:
            start -= self.flank
            end += self.flank
            flanked.append((start, end, strand))

        elif self.strandspecific:
            start_plus = start - self.flank
            end_min = end + self.flank
            flanked.append((start_plus, end, '+'))
            flanked.append((start, end_min, '-'))
        return flanked

    def create_bed(self, fn):
        out = str()
        
        for gene in self.genes:
            if gene in ['\n', '\r\n']:
                continue
            print(gene)
            chrom, _start, _end, _strand = self.get_max_gene_region(gene)
            flanked = self.flank_region((_start, _end), _strand)

            for region in flanked:
                start, end, strand = region
                out += f'{chrom}\t{start}\t{end}\t{strand}\t{gene}\t{end-start}\n'
        
        bed = pybedtools.BedTool(out, from_string=True)
        bed = bed.sort()
        bed.saveas(fn)
        return bed
